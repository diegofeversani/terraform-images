FROM alpine:latest

ARG TERRA=terraform
ARG TERRA_V=1.8.0
ARG DOCS=terraform-docs
ARG DOCS_V=0.17.0

#Instalar AWS-CLI
RUN apk add --no-cache aws-cli

#Instalar terraform
RUN apk add --update --virtual .deps --no-cache gnupg && \
    cd /tmp && \
    wget -q https://releases.hashicorp.com/${TERRA}/${TERRA_V}/${TERRA}_${TERRA_V}_linux_amd64.zip && \
    wget -q https://releases.hashicorp.com/${TERRA}/${TERRA_V}/${TERRA}_${TERRA_V}_SHA256SUMS && \
    wget -q https://releases.hashicorp.com/${TERRA}/${TERRA_V}/${TERRA}_${TERRA_V}_SHA256SUMS.sig && \
    wget -qO- https://www.hashicorp.com/.well-known/pgp-key.txt | gpg --import && \
    gpg --verify ${TERRA}_${TERRA_V}_SHA256SUMS.sig ${TERRA}_${TERRA_V}_SHA256SUMS && \
    grep ${TERRA}_${TERRA_V}_linux_amd64.zip ${TERRA}_${TERRA_V}_SHA256SUMS | sha256sum -c && \
    unzip /tmp/${TERRA}_${TERRA_V}_linux_amd64.zip -d /tmp && \
    mv /tmp/${TERRA} /usr/local/bin/${TERRA} && \
    apk del .deps

#Instalar terraform-docs
RUN cd /tmp && \
    wget -qO ./${DOCS}.tar.gz https://${DOCS}.io/dl/v${DOCS_V}/${DOCS}-v${DOCS_V}-$(uname)-amd64.tar.gz && \
    tar -xzf ${DOCS}.tar.gz && \
    chmod +x ${DOCS} && \
    mv ${DOCS} /usr/local/bin/

RUN rm tmp/*

ENTRYPOINT [ "/bin/sh", "-l", "-c" ]